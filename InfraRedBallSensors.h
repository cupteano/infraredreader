
#ifndef InfraReadBallSensors_h
#define InfraReadBallSensors_h

#include "MovingAverage.h"

#include "arduino.h"

namespace Cupteano {

    class InfraRedBallSensors {

    private:
        static constexpr int readTimes      = 127;
        static constexpr int numberOfIRPins = 12;
        static constexpr int intervalOfIR   = 360 / numberOfIRPins;
        
        int count = 0;

        MovingAverage xSmoother = MovingAverage(15);
        MovingAverage ySmoother = MovingAverage(15);

        uint8_t scalar[numberOfIRPins];
        float vectorX = 0;
        float vectorY = 0;

    public:
        InfraRedBallSensors();
        void refresh(uint16_t);
        void clacVector();
        uint16_t getAngle();
        uint16_t getDistance();

    };
}

#endif
