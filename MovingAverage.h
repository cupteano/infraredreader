
#ifndef MovingAverage_h
#define MovingAverage_h

namespace Cupteano {

    class MovingAverage {
        
    private:
        int times;
        float result = 0;
        int *dataBuffer;
    
    public:
        MovingAverage(int);
        int smooth(int);
        ~MovingAverage();
        
    };

}

#endif
