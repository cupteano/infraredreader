
#include "MovingAverage.h"

using namespace Cupteano;

MovingAverage::MovingAverage(int _times) : times(_times), dataBuffer(new int[_times])
{
    for (int i = 0; i < times; i++) dataBuffer[i] = 0;
}

int MovingAverage::smooth(int latest)
{
    dataBuffer[times - 1] = latest;
    result = result - ((float)dataBuffer[0] / (float)times) + ((float)dataBuffer[times - 1] / (float)times);
    
    for (int i = 0; i < times - 1; i++) dataBuffer[i] = dataBuffer[i + 1];
    dataBuffer[times - 1] = 0;
    
    return (int)result;
}

MovingAverage::~MovingAverage()
{
    delete dataBuffer;
}

