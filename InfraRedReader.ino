
#include "InfraRedBallSensors.h"

//#define PRINT_RAWDATA
//#define PRINT_ANGLE

const int16_t speedOfCommunication = 9600;

Cupteano::InfraRedBallSensors ir;

void setup()
{
    Serial.begin(speedOfCommunication);
}

void loop()
{   
    uint8_t firstHarf  = PINC << 2 >> 2;
    uint8_t SecondHarf = PINB << 2 >> 2;

    uint16_t IR12Data = SecondHarf << 6 | firstHarf;

    ir.refresh(IR12Data);

    #ifdef PRINT_RAWDATA
        for (int i = 0; i < 12; i++) Serial.print(IR12Data & 1 << i ? 0 : 1);
        Serial.println();
    #endif

    #ifdef PRINT_ANGLE
        Serial.println(ir.getAngle());
    #endif

    if (Serial.available()) {
        if (Serial.read() == 'A') {
            
            uint16_t angle = ir.getAngle();
            
            uint8_t upperBitsOfAngle = angle >> 8;
            uint8_t lowerBitsOfAngle = angle & 0x00ff;
            
            Serial.write(upperBitsOfAngle);
            Serial.write(lowerBitsOfAngle);
            
        } else {
                        
            uint16_t distance = ir.getDistance();
                
            uint8_t upperBitsOfDistance = distance >> 8;
            uint8_t lowerBitsOfDistance = distance & 0x00ff;
            
            Serial.write(upperBitsOfDistance);
            Serial.write(lowerBitsOfDistance);
            
        }
    }

}
