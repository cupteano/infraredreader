
#include "InfraRedBallSensors.h"

#include "CommonObjects.h"

using namespace Cupteano;

InfraRedBallSensors::InfraRedBallSensors()
{
    
}

void InfraRedBallSensors::refresh(uint16_t IR12Data)
{
    if (0 == count) {
        for (int i = 0; i < numberOfIRPins; i++) scalar[i] = 0;
        count++;
    } else if (readTimes != count) {
        for (int i = 0; i < numberOfIRPins; i++) scalar[i] += IR12Data & (1 << i) ? 0 : 1;
        count++;
    } else {
        clacVector();
        count = 0;
    }
}

void InfraRedBallSensors::clacVector()
{
    vectorX = vectorY = 0;

    for (int i = 0; i <= numberOfIRPins - 1; i++) {
        vectorX += scalar[i] * cos(radians(i * intervalOfIR));
        vectorY += scalar[i] * sin(radians(i * intervalOfIR));
    }

    vectorX = xSmoother.smooth(vectorX);
    vectorY = ySmoother.smooth(vectorY);
}

uint16_t InfraRedBallSensors::getAngle()
{
    return Cupteano::safeDegrees(degrees(atan2(vectorY, vectorX)), 0, 360);
}

uint16_t InfraRedBallSensors::getDistance()
{
    return sqrt((vectorX) * (vectorX) + (vectorY) * (vectorY));
}

